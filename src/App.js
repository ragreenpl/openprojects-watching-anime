import axios from "axios";
import { useState, useEffect, useRef } from "react";
import { Container, Row, Col, Nav, Button, Collapse } from "react-bootstrap";
import logo from "./assets/logo_trans.png";
import Guessing from "./Guessing";
import Ranking from "./Ranking";
import './styles.css';

const App = () => {
  const [series, setSeries] = useState([]);
  const [open, setOpen] = useState(1);
  const [openImage, setOpenImage] = useState(null);
  const ref = useRef(null);
  const [online, setOnline] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const [showNames, setShowNames] = useState(false);
  const [darkMode, setDarkMode] = useState(true);

  useEffect(() => {
    document.body.classList.toggle('dark-mode');
  }, [darkMode]);

  useEffect(() => {
    axios
      .get("data.json")
      .then((response) => {
        setSeries(response.data);
        setIsLoading(false);
      })
      .catch(() => setOnline(false));
  }, []);

  const loadFile = () => {
    const { current } = ref;
    const { files } = current;
    const file = files[0];
    const reader = new FileReader();

    reader.onload = (e) => {
      setSeries(JSON.parse(reader.result));
      setIsLoading(false);
    };

    reader.readAsText(file);
  };

  if (!online && series.length === 0) {
    return (
      <div style={{ display: "flex", flexDirection: "column", margin: "16px" }}>
        <label style={{ fontWeight: 600 }}>Wskaż plik watchingu</label>
        <input type="file" ref={ref} onChange={loadFile} />
      </div>
    );
  }

  return (
    <>
      {openImage && (
        <div
          onClick={() => setOpenImage(null)}
          style={{
            position: "fixed",
            height: "100vh",
            width: "100%",
            top: 0,
            left: 0,
            overflow: "hidden",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0, 0, 0, 0.6)",
            cursor: "zoom-out",
            zIndex: 666,
          }}
        >
          <img src={openImage} alt="Zrzut" style={{ maxWidth: "90%" }} />
        </div>
      )}
      <Container className="mt-2" style={{ minHeight: "100vh" }}>
        <Row>
          <Col sm={3}>
            <Nav
              className="flex-column"
              style={{
                position: "sticky",
                top: 0,
                width: "100%",
                maxHeight: '99vh',
                flexWrap: 'nowrap',
                overflowY: 'auto'
              }}
            >
              {series.map((item, key) => {
                return (
                  <Button
                    variant={open === key + 1 ? "success" : "primary"}
                    className="m-2 d-flex flex-column align-items-center"
                    key={key}
                    onClick={() => {
                      window.scrollTo({
                        top: 0,
                      });
                      setOpen(key + 1);
                    }}
                  >
                    <h5>Propozycja {key + 1}</h5>
                    {showNames && <small>{item.title}</small>}
                  </Button>
                );
              })}

              <hr />

              <Button
                variant={open === "ranking" ? "success" : "primary"}
                className="m-2"
                onClick={() => {
                  window.scrollTo({
                    top: 0,
                  });
                  setOpen("ranking");
                }}
              >
                Ranking
              </Button>

              <Button
                variant={open === "summary" ? "success" : "primary"}
                className="m-2"
                onClick={() => {
                  window.scrollTo({
                    top: 0,
                  });
                  setOpen("summary");
                }}
              >
                Zgaduj zgadula?
              </Button>

              <hr />

              <Button
                variant={!showNames ? "success" : "danger"}
                className="m-2"
                onClick={() => setShowNames(!showNames)}
              >
                {showNames
                  ? "Ukryj nazwy propozycji"
                  : "Pokaż nazwy propozycji"}
              </Button>
              
              <Button
                variant={!darkMode ? "success" : "danger"}
                className="m-2"
                onClick={() => setDarkMode(!darkMode)}
              >
                {darkMode
                  ? "Wyłącz darkmode"
                  : "Włącz darkmode"}
              </Button>
            </Nav>
          </Col>

          <Col sm={9}>
            {series.map((item, key) => {
              return (
                <Collapse in={open === key + 1} key={key}>
                  <div>
                    <h1>{item.title}</h1>
                    {item.titleEN && <small>{item.titleEN}</small>}
                    <h3 className="mt-4">Ilość odcinków: {item.length}</h3>
                    <h4 className="mb-4">Gatunki: {item.genres}</h4>
                    {item.paragraphs.map((paragraph, pkey) => {
                      return <p key={pkey}>{paragraph}</p>;
                    })}
                    {item.images.map((image, ikey) => {
                      return (
                        <img
                          key={ikey}
                          src={image}
                          alt={`Zrzut ${ikey}`}
                          className="m-2 img-fluid"
                          style={{ cursor: "zoom-in" }}
                          onClick={() => setOpenImage(image)}
                        />
                      );
                    })}
                  </div>
                </Collapse>
              );
            })}

            <Collapse in={open === "ranking"}>
              <div>{!isLoading && <Ranking series={series} darkMode={darkMode} />}</div>
            </Collapse>

            <Collapse in={open === "summary"}>
              <div>
                <Guessing series={series} darkMode={darkMode} />
              </div>
            </Collapse>
          </Col>
        </Row>
      </Container>

      <footer className="mt-4 bg-dark d-flex justify-content-center align-items-center flex-column pb-3">
        <a
          target="_blank"
          rel="noreferrer"
          href="https://ragreen.pl"
          className="text-decoration-none"
        >
          <img src={logo} alt="RaGreen.pl" height="250" width="250" />
          <h3 className="text-white text-center">RaGreen.pl</h3>
        </a>
      </footer>
    </>
  );
};

export default App;
