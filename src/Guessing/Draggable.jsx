import { useDraggable } from "@dnd-kit/core";
import React from "react";
import { Button } from "react-bootstrap";

const Draggable = ({ disabled, index, item }) => {
  const { setNodeRef, listeners, attributes } = useDraggable({
    id: disabled ? 'disabled' : "author_" + index,
  });

  if (disabled) {
    return <Button variant="primary">{item}</Button>;
  }

  return (
    <div ref={setNodeRef} className="p-1">
      <Button variant="primary" {...listeners} {...attributes}>
        {item}
      </Button>
    </div>
  );
};

export default Draggable;
