import { DndContext, DragOverlay } from "@dnd-kit/core";
import React, { useEffect, useRef, useState } from "react";
import { Row, Col, Button } from "react-bootstrap";
import Draggable from "./Draggable";
import Droppable from "./Droppable";
import { toPng } from "html-to-image";
import clsx from "classnames";

const Guessing = ({ series, darkMode }) => {
  const [activeId, setActiveId] = useState(null);
  const [authors, setAuthors] = useState([]);
  const [picks, setPicks] = useState({});
  const [checked, setChecked] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const ref = useRef(null);

  useEffect(() => {
    setDisabled(Object.values(picks).includes(null));
  }, [picks]);

  useEffect(() => {
    const authors = series
      .map((item) => {
        return {
          item,
          sort: Math.random(),
        };
      })
      .sort((a, b) => a.sort - b.sort)
      .map(({ item }) => item.author);

    setAuthors(authors);
  }, [series]);

  useEffect(() => {
    let picks = {};

    series.forEach((_, key) => {
      picks[key] = null;
    });

    setPicks(picks);
  }, [series]);

  const onDragStart = (e) => {
    setActiveId(e.active.id.replace("author_", ""));
  };

  const onDropEnd = (e) => {
    setActiveId(null);

    if (!e.over) {
      return;
    }

    const author = parseInt(e.active.id.replace("author_", ""));
    const place = parseInt(e.over.id.replace("author_drop_", ""));

    const index = Object.values(picks).findIndex((item) => item === author);
    let tempPicks = {
      ...picks,
      [place]: author,
    };

    if (index !== -1) {
      tempPicks[index] = null;
    }

    setPicks(tempPicks);
  };

  const downloadResults = () => {
    if (ref.current === null) {
      return;
    }

    toPng(ref.current, { cacheBust: true })
      .then((dataUrl) => {
        const link = document.createElement("a");
        link.download = "wyniki_zgaduli.png";
        link.href = dataUrl;
        link.click();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <DndContext onDragStart={onDragStart} onDragEnd={onDropEnd}>
        <div
          ref={ref}
          className={clsx('p-2', {
            "bg-white": !darkMode,
            "dark-mode": darkMode,
          })}
        >
          {series.map((item, key) => {
            return (
              <div key={key}>
                <Row>
                  <Col sm={6}>
                    <small>Propozycja {key + 1}</small>
                    <h5>{item.title}</h5>
                  </Col>

                  <Col sm={6}>
                    <Droppable index={key}>
                      {picks[key] !== null && (
                        <Draggable
                          item={authors[picks[key]]}
                          index={picks[key]}
                        />
                      )}
                    </Droppable>
                    {checked && (
                      <span
                        className={
                          item.author === authors[picks[key]]
                            ? "text-success"
                            : "text-danger"
                        }
                      >
                        Poprawna odpowiedź: {item.author}
                      </span>
                    )}
                  </Col>
                </Row>
                <hr />
              </div>
            );
          })}
        </div>

        <div className="d-flex">
          {authors.map((item, skey) => {
            if (
              Object.values(picks).find((temp) => temp === skey) !== undefined
            ) {
              return null;
            }

            return <Draggable key={skey} item={item} index={skey} />;
          })}
        </div>

        <DragOverlay>
          {activeId && <Draggable disabled item={authors[activeId]} />}
        </DragOverlay>
      </DndContext>

      <Row className="mt-4">
        <Col sm={6} className="mx-auto">
          <Button
            variant="success"
            onClick={() => setChecked(true)}
            disabled={disabled}
            className='w-100'
          >
            Sprawdź
          </Button>

          <Button
            className='mt-2 w-100'
            variant="success"
            disabled={disabled}
            onClick={downloadResults}
          >
            Pobierz
          </Button>
        </Col>
      </Row>
    </>
  );
};

export default Guessing;
