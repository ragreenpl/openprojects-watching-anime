import { useDroppable } from "@dnd-kit/core";
import clsx from "classnames";
import React from "react";

const Droppable = ({ index, children }) => {
  const { setNodeRef, isOver } = useDroppable({
    id: "serie_drop_" + index,
  });

  return (
    <div
      style={{ minHeight: 40 }}
      ref={setNodeRef}
      className={clsx("w-100", {
        "border-success": isOver,
        "border-2": isOver,
        "border": isOver
      })}
    >{children}</div>
  );
};

export default Droppable;
