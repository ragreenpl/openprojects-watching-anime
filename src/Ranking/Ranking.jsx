import React, { useEffect, useRef, useState } from "react";
import Draggable from "./Draggable";
import { DndContext, DragOverlay } from "@dnd-kit/core";
import Droppable from "./Droppable";
import { Button, Col, Row } from "react-bootstrap";
import { toPng } from "html-to-image";
import clsx from "classnames";

const Ranking = ({ series, darkMode }) => {
  const [activeId, setActiveId] = useState(null);
  const [ranking, setRanking] = useState({});
  const [disabled, setDisabled] = useState(true);
  const ref = useRef(null);

  useEffect(() => {
    const result = Object.entries(ranking)
      .map((entry) => entry[1])
      .find((item) => item === null);

    setDisabled(result === null);
  }, [ranking]);

  useEffect(() => {
    const ranking = {};
    series.forEach((_, key) => {
      ranking[key] = null;
    });

    setRanking(ranking);
  }, [series]);

  const onDragStart = (e) => {
    setActiveId(e.active.id.replace("serie_", ""));
  };

  const onDropEnd = (e) => {
    setActiveId(null);

    if (!e.over) {
      return;
    }

    const serie = parseInt(e.active.id.replace("serie_", ""));
    const place = parseInt(e.over.id.replace("serie_drop_", ""));

    setRanking({
      ...ranking,
      [serie]: place + 1,
    });
  };

  const downloadResults = () => {
    if (ref.current === null) {
      return;
    }

    toPng(ref.current, { cacheBust: true })
      .then((dataUrl) => {
        const link = document.createElement("a");
        link.download = "wyniki_watchingu.png";
        link.href = dataUrl;
        link.click();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <DndContext onDragStart={onDragStart} onDragEnd={onDropEnd}>
      <ol
        ref={ref}
        className={clsx("p-2", {
          "bg-white": !darkMode,
          "dark-mode": darkMode,
        })}
      >
        {[...Array(series.length).keys()].map((key) => {
          return (
            <li key={key}>
              <Droppable index={key}>
                {Object.entries(ranking).map((entry) => {
                  const [serie, place] = entry;

                  if (place !== key + 1) {
                    return null;
                  }

                  return (
                    <Draggable key={serie} item={series[serie]} index={serie} />
                  );
                })}
              </Droppable>
              <hr />
            </li>
          );
        })}
      </ol>

      <div className="d-flex flex-wrap">
        {series.map((item, skey) => {
          if (ranking[skey]) {
            return null;
          }

          return <Draggable key={skey} item={item} index={skey} />;
        })}
      </div>

      <DragOverlay>
        {activeId && (
          <Draggable disabled item={series[activeId]} index={activeId} />
        )}
      </DragOverlay>

      <Row className="mt-4">
        <Col sm={3} className="mx-auto">
          <Button
            variant="success"
            disabled={disabled}
            onClick={downloadResults}
            className="w-100"
          >
            Pobierz
          </Button>
        </Col>
      </Row>
    </DndContext>
  );
};

export default Ranking;
